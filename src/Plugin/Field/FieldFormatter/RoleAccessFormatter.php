<?php

namespace Drupal\entity_access_by_role\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'role_access_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "role_access_formatter",
 *   module = "entity_access_by_role",
 *   label = @Translation("Simple formatter"),
 *   field_types = {
 *     "role_access"
 *   }
 * )
 */
class RoleAccessFormatter extends OptionsDefaultFormatter {

}
