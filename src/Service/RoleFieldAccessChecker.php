<?php

namespace Drupal\entity_access_by_role\Service;

use _PHPStan_76800bfb5\Nette\Neon\Entity;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

class RoleFieldAccessChecker {

  public function access(EntityInterface $entity, $operation, $account) {

    if (!$this->requiresAccessCheck($entity, $operation)) {
      return AccessResult::neutral();
    }

    // Create an array of any role_access fields.
    $roleAccessFields = array_filter($entity->getFieldDefinitions(), function ($fieldDefinition) {

      if ($fieldDefinition->getType() == "role_access") {
        return $fieldDefinition;
      }
    });

    // If there are no fields on the entity, there's no role access check.
    if (empty($roleAccessFields)) {
      AccessResult::neutral();
    }

    // Loop through any instances of the field and grab the values.
    foreach ($roleAccessFields as $roleAccessfieldDefinition) {

      if ($this->userHasAllowedRole($account, $roleAccessfieldDefinition)) {
        return AccessResult::neutral();
      }

      // Check the value against the current user.
      $roleAccessValues = $entity->{$roleAccessfieldDefinition->getName()}->getValue();

      // If there are no values, field is available but nothing selected,
      if (empty($roleAccessValues)) {
        return AccessResult::neutral();
      }

      // Create a flat array of the role_access field values.
      $roleAccessValues = array_map(function ($valueArray) {

        return $valueArray['value'];
      }, $roleAccessValues);
      // If there are no matches between arrays, forbid.
      // Included account id == 1 as User 1 was unable to view paragraphs.
      if (!array_intersect($roleAccessValues, $account->getRoles())) {
        return AccessResult::forbidden("The user:" . $account->id() . "does not have the required role to view this entity (entity_access_by_role)");
      }
      return AccessResult::neutral();

    }


  }

  protected function requiresAccessCheck(EntityInterface $entity, $operation) {

    // Ensure the entity type is fieldable. and operation is view
    return ($entity->getEntityType()
        ->entityClassImplements(FieldableEntityInterface::class) &&
      $operation == "view");

  }

  protected function userHasAllowedRole($account, $fieldDefinition) {

    $alwaysAllowedRoles = $this->getAlwaysAllowedRoles($fieldDefinition);

    return (bool) array_intersect($alwaysAllowedRoles, $account->getRoles());

  }

  protected function getAlwaysAllowedRoles($definition) {

    $alwaysAllowedValues = $definition->getSetting("always_allowed");

    if (empty($alwaysAllowedValues)) {
      return [];
    }

    $alwaysAllowed = array_filter($alwaysAllowedValues, function ($role) {

      return !empty($role);
    });

    return $alwaysAllowed;

  }

}
